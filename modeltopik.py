from db import db

class Topik(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(50), nullable=False)
    keterangan = db.Column(db.String(500), nullable=False)

