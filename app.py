# app.py
from flask import Flask, request, render_template, redirect, url_for, session
from db import db
from matakuliah import get_matakuliahs, add_matakuliah, edit_matakuliah, delete_matakuliah
from modelmatakuliah import MataKuliah
from modeltopik import Topik
from topik import get_topiks, add_topik, edit_topik, delete_topik
from modelmahasiswa import Mahasiswa
from mahasiswa import get_mahasiswas, add_mahasiswa, edit_mahasiswa, delete_mahasiswa
from modeltraining import Training
from training import get_trainings, add_training, edit_training, delete_training
from modelnilaitraining import NilaiTraining
from nilaitraining import add_nilaitraining, edit_nilaitraining, get_datatrainingwithnilai
from collections import defaultdict
import math


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['SECRET_KEY'] = 'your_secret_key' 
db.init_app(app)

with app.app_context():
    db.create_all()

# Rute untuk halaman login
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['NIM']
        password = request.form['password']
        if username == 'admin' and password == 'admin':
            session['logged_in'] = True
            session['username'] = 'admin'
            return redirect(url_for('index'))
        
        user = Mahasiswa.query.filter_by(nim=username).first()
        if user and password == user.password:
            session['logged_in'] = True
            session['nim'] = user.nim
            session['role'] = 'user'
            return redirect(url_for('home'))
    return render_template('login.html')

# Rute untuk halaman utama
@app.route('/')
def index():
    return render_template('base.html')

# Rute untuk logou
@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('login'))




# ROUTE MATA KULIAH
@app.route('/matakuliah')
def matakuliah_index():
    matakuliahs = get_matakuliahs()
    return render_template('matakuliah.html', matakuliahs=matakuliahs)

@app.route('/matakuliah/add', methods=['GET', 'POST'])
def add_matakuliah_route():
    if request.method == 'POST':
        nama = request.form['nama']
        add_matakuliah(nama)
        print(nama)
        return redirect(url_for('matakuliah_index'))

    return render_template('add_matakuliah.html')

@app.route('/matakuliah/edit/<int:matakuliah_id>', methods=['GET', 'POST'])
def edit_matakuliah_route(matakuliah_id):
    matakuliah = MataKuliah.query.get(matakuliah_id)
    matakuliahs = get_matakuliahs()

    if request.method == 'POST':
        nama = request.form['nama']
        edit_matakuliah(matakuliah_id, nama)
        return redirect(url_for('matakuliah_index'))

    return render_template('matakuliahedit.html', matakuliahs=matakuliahs, matakuliah=matakuliah)

@app.route('/matakuliah/delete/<int:matakuliah_id>')
def delete_matakuliah_route(matakuliah_id):
    delete_matakuliah(matakuliah_id)
    return redirect(url_for('matakuliah_index'))




# ROUTE HALAMAN TOPIK
@app.route('/topik')
def topik_index():
    topiks = get_topiks()
    return render_template('topik.html', topiks=topiks)

@app.route('/topik/add', methods=['GET', 'POST'])
def add_topik_route():
    if request.method == 'POST':
        nama = request.form['nama']
        keterangan = request.form['keterangan']
        add_topik(nama, keterangan)
        return redirect(url_for('topik_index'))

    return render_template('topik.html')

@app.route('/topik/edit/<int:topik_id>', methods=['GET', 'POST'])
def edit_topik_route(topik_id):
    topik = Topik.query.get(topik_id)
    topiks = get_topiks()

    if request.method == 'POST':
        nama = request.form['nama']
        keterangan = request.form['keterangan']
        edit_topik(topik_id, nama, keterangan)
        return redirect(url_for('topik_index'))

    return render_template('topikedit.html', topik=topik, topiks=topiks)

@app.route('/topik/delete/<int:topik_id>')
def delete_topik_route(topik_id):
    delete_topik(topik_id)
    return redirect(url_for('topik_index'))



# ROUTE MAHASISWA
@app.route('/mahasiswa')
def mahasiswa_index():
    mahasiswas = get_mahasiswas()
    return render_template('mahasiswa.html', mahasiswas=mahasiswas)

@app.route('/mahasiswa/add', methods=['GET', 'POST'])
def add_mahasiswa_route():
    if request.method == 'POST':
        nama = request.form['nama']
        nim = request.form['nim']
        password = request.form['password'] 
        add_mahasiswa(nama, nim, password)
        return redirect(url_for('mahasiswa_index'))

    return render_template('mahasiswa.html')

@app.route('/mahasiswa/edit/<int:mahasiswa_id>', methods=['GET', 'POST'])
def edit_mahasiswa_route(mahasiswa_id):
    mahasiswa = Mahasiswa.query.get(mahasiswa_id)
    mahasiswas = get_mahasiswas()

    if request.method == 'POST':
        nama = request.form['nama']
        nim = request.form['nim']
        edit_mahasiswa(mahasiswa_id, nama, nim)
        return redirect(url_for('mahasiswa_index'))

    return render_template('mahasiswaedit.html', mahasiswa=mahasiswa, mahasiswas=mahasiswas)

@app.route('/mahasiswa/delete/<int:mahasiswa_id>')
def delete_mahasiswa_route(mahasiswa_id):
    delete_mahasiswa(mahasiswa_id)
    return redirect(url_for('mahasiswa_index'))



# ROUTE TRAINING

def get_nilai(training_id, mata_kuliah_id):
    nilai = NilaiTraining.query.filter_by(training_id=training_id, matkul_id=mata_kuliah_id).first()
    return nilai.nilai if nilai else ''

def get_nilai_id(training_id, mata_kuliah_id):
    nilaitraining = NilaiTraining.query.filter_by(training_id=training_id, matkul_id=mata_kuliah_id).first()
    return nilaitraining.id if nilaitraining else ''


@app.route('/training')
def training_index():
    trainings = get_trainings()
    topiks = get_topiks()
    return render_template('training.html', trainings=trainings, topiks=topiks)

@app.route('/training/add', methods=['GET', 'POST'])
def add_training_route():
    if request.method == 'POST':
        nama = request.form['nama']
        nim = request.form['nim']
        topik_id = request.form['topik_id'] 
        add_training(nama, nim, topik_id)
        return redirect(url_for('training_index'))

    return render_template('training.html')

@app.route('/training/edit/<int:training_id>', methods=['GET', 'POST'])
def edit_training_route(training_id):
    topiks = get_topiks()
    matakuliahs = get_matakuliahs()
    training = Training.query.get(training_id)
    if request.method == 'POST':
        # Ambil data dari formulir
        training_id = request.form['training_id']
        topik_id = request.form['topik_id']
        nama = request.form['nama']
        nim = request.form['nim']

        # Cek apakah data training sudah ada
        existing_training = Training.query.get(training_id)

        if existing_training:
            # Jika data training sudah ada, update nilai
            existing_training.training_id = training_id
            existing_training.topik_id = topik_id
            existing_training.nim = nim
            existing_training.nama = nama
            edit_training(training_id, nama, nim, topik_id)
        else:
            # Jika data training belum ada, tambahkan data baru
            add_training(nama, nim, topik_id)

        # Commit perubahan atau penambahan data training

        # Ambil ID training yang baru ditambahkan atau sudah ada

        # Loop untuk menyimpan atau update nilai mata kuliah
        mata_kuliah_prefix = 'mata_kuliah_'
        for key, value in request.form.items():
            if key.startswith(mata_kuliah_prefix):
                matkul_id = int(key[len(mata_kuliah_prefix):])
                training_id = request.form['training_id']
                data_id_nilai = get_nilai_id(training_id, matkul_id)
                existing_nilai = NilaiTraining.query.filter_by(training_id=training_id, matkul_id=matkul_id).first()

                if existing_nilai:
                    # Jika data nilai sudah ada, update nilai
                    edit_nilaitraining(data_id_nilai, value)
                else:
                    # Jika data nilai belum ada, tambahkan data baru
                    add_nilaitraining(training_id, matkul_id, value)

        # Commit perubahan atau penambahan data nilai mata kuliah
        db.session.commit()

        return redirect(url_for('edit_training_route', training_id=training_id))

    # Jika method adalah GET, render halaman edit_training
    return render_template('trainingedit.html', training=training,
                           topiks=topiks, mata_kuliahs=matakuliahs,
                           get_nilai=get_nilai, get_nilai_id=get_nilai_id)

@app.route('/training/delete/<int:training_id>')
def delete_training_route(training_id):
    delete_training(training_id)
    return redirect(url_for('training_index'))








# HALAMAN UNTUK MAHASISWA



@app.route('/home')
def home():
    return render_template('basemahasiswa.html')


def euclidean_distance(data1, data2):
    return math.sqrt(sum((x - y) ** 2 for x, y in zip(data1, data2)))

@app.route('/rekomendasi', methods=['GET', 'POST'])
def rekomendasi():
    mahasiswa_id = 1
    matakuliahs = get_matakuliahs()
    mahasiswa = Mahasiswa.query.get(mahasiswa_id)
    data_lama = get_datatrainingwithnilai()
    data_lama_formatted = defaultdict(dict)
    for data in data_lama:
        mahasiswa_id = data['id']
        if mahasiswa_id not in data_lama_formatted:
            data_lama_formatted[mahasiswa_id]['id'] = data['id']
            data_lama_formatted[mahasiswa_id]['nama'] = data['nama']
            data_lama_formatted[mahasiswa_id]['nim'] = data['nim']
            data_lama_formatted[mahasiswa_id]['topik_id'] = data['topik_id']
            data_lama_formatted[mahasiswa_id]['nilai'] = []

        # Tambahkan nilai ke dalam daftar nilai
        data_lama_formatted[mahasiswa_id]['nilai'].append(data['nilai'])
    data_lama_formatted_list = list(data_lama_formatted.values())
    nilai_mapping = {'A': 100, 'B': 90, 'C': 70, 'D': 60}
    for data in data_lama_formatted_list:
        data['nilai'] = [nilai_mapping[nilai] for nilai in data['nilai']]
    print(data_lama_formatted_list)
    if request.method == 'POST':
        # Ambil data dari formulir
        distances = []
        nilai_mata_kuliah = {}
        mata_kuliah_prefix = 'mata_kuliah_'
        for key, value in request.form.items():
            if key.startswith(mata_kuliah_prefix):
            # Ambil ID mata kuliah dari nama input
                matkul_id = int(key[len(mata_kuliah_prefix):])
                # Tambahkan nilai ke dalam array nilai mata kuliah sesuai dengan ID mata kuliah
                if matkul_id in nilai_mata_kuliah:
                    nilai_mata_kuliah[matkul_id].append(value)
                else:
                    nilai_mata_kuliah[matkul_id] = [value]
        nilai_data_baru_mentah = [nilai for nilai_list in nilai_mata_kuliah.values() for nilai in nilai_list]
        nilai_data_baru = [nilai_mapping[nilai] for nilai in nilai_data_baru_mentah]
        nilai_data_lama = [data['nilai'] for data in data_lama_formatted_list]
        print(nilai_data_baru)
        print(nilai_data_lama)
        distances = [euclidean_distance(data, nilai_data_baru) for data in nilai_data_lama]
        
        # Temukan Euclidean distance terkecil
        min_distance = min(distances)
        # Temukan indeks Euclidean distance terkecil
        min_distance_index = distances.index(min_distance)

        # Ambil data lama yang sesuai dengan indeks terkecil
        nearest_data = data_lama[min_distance_index]

        # Ambil nama dan topik_id dari data lama yang paling dekat
        nama_terdekat = nearest_data['nama']
        topik_terdekat = nearest_data['topik_id']
        topik = Topik.query.get(topik_terdekat)
        print(topik.nama)
        # Cetak hasil
        print(distances)
        print(min_distance_index)
        print("Nama terdekat:", nama_terdekat)
        print("Topik terdekat:", topik_terdekat)
        print("Euclidean Distance terkecil:", min_distance)

        return render_template('hasilrekomendasi.html', nama_terdekat=nama_terdekat, topik=topik.nama, euclidean_distance=min_distance)

    # Jika method adalah GET, render halaman edit_training
    return render_template('rekomendasi.html', mata_kuliahs=matakuliahs, mahasiswa=mahasiswa)



# END

if __name__ == '__main__':
    app.run(debug=True)











