from db import db

class Mahasiswa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(50), nullable=False)
    nim = db.Column(db.String(500), nullable=False)
    password = db.Column(db.String(500), nullable=False)

