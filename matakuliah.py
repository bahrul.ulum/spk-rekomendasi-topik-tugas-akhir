# matakuliah.py
from flask import render_template, request, redirect, url_for
from app import db
from modelmatakuliah import MataKuliah

def get_matakuliahs():
    return MataKuliah.query.all()

def add_matakuliah(nama):
    new_matakuliah = MataKuliah(nama=nama)
    db.session.add(new_matakuliah)
    db.session.commit()

def edit_matakuliah(matakuliah_id, nama):
    matakuliah = MataKuliah.query.get(matakuliah_id)
    matakuliah.nama = nama
    db.session.commit()

def delete_matakuliah(matakuliah_id):
    matakuliah = MataKuliah.query.get(matakuliah_id)
    db.session.delete(matakuliah)
    db.session.commit()
