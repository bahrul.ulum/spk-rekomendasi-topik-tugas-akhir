# mahasiswa.py
from app import db
from modelmahasiswa import Mahasiswa

def get_mahasiswas():
    return Mahasiswa.query.all()

def add_mahasiswa(nama, nim, password):
    new_mahasiswa = Mahasiswa(nama=nama, nim=nim, password=password)
    db.session.add(new_mahasiswa)
    db.session.commit()

def edit_mahasiswa(mahasiswa_id, nama, nim, password):
    mahasiswa = Mahasiswa.query.get(mahasiswa_id)
    mahasiswa.nama = nama
    mahasiswa.nim = nim
    mahasiswa.password = password
    db.session.commit()

def delete_mahasiswa(mahasiswa_id):
    mahasiswa = Mahasiswa.query.get(mahasiswa_id)
    db.session.delete(mahasiswa)
    db.session.commit()
