# topik.py
from flask import render_template, request, redirect, url_for
from app import db
from modeltopik import Topik

def get_topiks():
    return Topik.query.all()

def add_topik(nama, keterangan):
    new_topik = Topik(nama=nama, keterangan=keterangan)
    db.session.add(new_topik)
    db.session.commit()

def edit_topik(topik_id, nama, keterangan):
    topik = Topik.query.get(topik_id)
    topik.nama = nama
    topik.keterangan = keterangan
    db.session.commit()

def delete_topik(topik_id):
    topik = Topik.query.get(topik_id)
    db.session.delete(topik)
    db.session.commit()
