# training.py
from app import db
from modeltraining import Training
from modeltopik import Topik

def get_trainings():
    return db.session.query(Training, Topik).join(Topik, Training.topik_id == Topik.id).all()

def add_training(nama, nim, topik_id):
    new_training = Training(nama=nama, nim=nim, topik_id=topik_id)
    db.session.add(new_training)
    db.session.commit()

def edit_training(training_id, nama, nim, topik_id):
    training = Training.query.get(training_id)
    training.nama = nama
    training.nim = nim
    training.topik_id = topik_id
    db.session.commit()

def delete_training(training_id):
    training = Training.query.get(training_id)
    db.session.delete(training)
    db.session.commit()
