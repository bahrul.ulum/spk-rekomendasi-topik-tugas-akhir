from sqlalchemy.orm import aliased

# training.py
from app import db
from modelnilaitraining import NilaiTraining
from modeltraining import Training

def get_nilaitrainings():
    return NilaiTraining.query.all()

def add_nilaitraining(training_id, matkul_id, nilai):
    new_training = NilaiTraining(training_id=training_id, matkul_id=matkul_id, nilai=nilai)
    db.session.add(new_training)
    db.session.commit()

def edit_nilaitraining(nilaitraining_id, nilai):
    nilaitraining = NilaiTraining.query.get(nilaitraining_id)
    nilaitraining.nilai = nilai
    db.session.commit()

def delete_nilaitraining(nilaitraining_id):
    nilaitraining = NilaiTraining.query.get(nilaitraining_id)
    db.session.delete(nilaitraining)
    db.session.commit()

def get_datatrainingwithnilai():
    training_alias = aliased(Training)
    nilaitraining_alias = aliased(NilaiTraining)
    joined_data = db.session.query(Training, NilaiTraining) \
                        .join(NilaiTraining, Training.id == NilaiTraining.training_id) \
                        .all()
    joined_data_list = []
    for training, nilaitraining in joined_data:
        training_data = {key: getattr(training, key) for key in training.__table__.columns.keys()}
        nilai_data = {key: getattr(nilaitraining, key) for key in nilaitraining.__table__.columns.keys() if key != 'id' and key != 'training_id'}
        joined_data_list.append({**training_data, **nilai_data})
    return joined_data_list
