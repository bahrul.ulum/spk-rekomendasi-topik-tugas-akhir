from db import db

class Training(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(50), nullable=False)
    nim = db.Column(db.String(500), nullable=False)
    topik_id = db.Column(db.String(500), db.ForeignKey('topik.id'), nullable=False)

