from db import db

class NilaiTraining(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    training_id = db.Column(db.Integer, db.ForeignKey('training.id'), nullable=False)
    matkul_id = db.Column(db.Integer, db.ForeignKey('mata_kuliah.id'), nullable=False)
    nilai = db.Column(db.String(100), nullable=False)

    training = db.relationship('Training', backref=db.backref('nilai_training', lazy=True))
    matkul = db.relationship('MataKuliah', backref=db.backref('nilai_training', lazy=True))

